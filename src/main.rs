mod chess;
#[allow(dead_code)]
#[allow(unused_imports)]
mod piece;
use crate::chess::*;
use crossterm::event::Event::Key;
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::layout::{Constraint, Direction, Layout, Rect};
use ratatui::style::{Color, Style};
use ratatui::{
    backend::Backend,
    backend::CrosstermBackend,
    text::Span,
    widgets::{Block, BorderType, Borders, List, ListItem, ListState, Paragraph},
    Frame, Terminal,
};
use std::error::Error;
use std::thread::sleep;
use std::time::{Duration, SystemTime};

fn main() -> Result<(), Box<dyn Error>> {
    let mut game = Chess::new();
    enable_raw_mode()?;
    execute!(std::io::stdout(), EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(std::io::stdout());
    let mut terminal = Terminal::new(backend)?;
    let result = run_game(&mut terminal, &mut game);

    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    if let Err(e) = result {
        println!("{}", e.to_string());
    }
    Ok(())
}
fn run_game<B: Backend>(
    terminal: &mut Terminal<B>,
    game: &mut Chess,
) -> Result<(), std::io::Error> {
    game.init();
    let mut table = [
        [7, 7, 7, 7, 7, 7, 7, 7],
        [7, 7, 7, 7, 7, 7, 7, 7],
        [7, 7, 7, 7, 7, 7, 7, 7],
        [7, 7, 7, 7, 7, 7, 7, 7],
        [7, 7, 7, 7, 7, 7, 7, 7],
        [7, 7, 7, 7, 7, 7, 7, 7],
        [7, 7, 7, 7, 7, 7, 7, 7],
        [7, 7, 7, 7, 7, 7, 7, 7],
    ];
    loop {
        let then = SystemTime::now();
        game.draw_table(&mut table);
        terminal.draw(|f| ui(f, game, &table))?;
        if let Key(key) = event::read()? {
            match game.get_mode() {
                Mode::Default => match key.code {
                    event::KeyCode::Esc => return Ok(()),
                    event::KeyCode::Char('s') => game.enter_select_mode(), //Enter Select Mode
                    _ => {}
                },
                Mode::Mark => match key.code {
                    event::KeyCode::Char('e') => game.exit_edit_mode(), // Exit to Default Mode
                    event::KeyCode::Up => game.move_edit(0),            // Move up
                    event::KeyCode::Right => game.move_edit(1),         // Move right
                    event::KeyCode::Down => game.move_edit(2),          // Move down
                    event::KeyCode::Left => game.move_edit(3),          // Move left
                    event::KeyCode::Tab | event::KeyCode::Char('m') => game.enter_mark_mode(), //Switch Mode
                    _ => {}
                },
                Mode::Select => match key.code {
                    event::KeyCode::Char('e') => game.exit_edit_mode(), // Exit to Default Mode
                    event::KeyCode::Up => game.move_edit(0),            // Move up
                    event::KeyCode::Right => game.move_edit(1),         // Move right
                    event::KeyCode::Down => game.move_edit(2),          // Move down
                    event::KeyCode::Left => game.move_edit(3),          // Move left
                    event::KeyCode::Char('s') => game.enter_select_mode(), //Switch Mode
                    _ => {}
                },
                _ => {}
            }
        }
        if then.elapsed().unwrap() < Duration::from_millis(40) {
            sleep(Duration::from_millis(40) - then.elapsed().unwrap());
        }
    }
}
fn ui<B: Backend>(f: &mut Frame<B>, game: &mut Chess, table: &[[i32; 8]; 8]) {
    let main_sq = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(80), Constraint::Min(3)].as_ref())
        .split(f.size());
    let ui_table_block = Block::default()
        .title("Game")
        .borders(Borders::ALL)
        .border_type(BorderType::Rounded);
    f.render_widget(ui_table_block, main_sq[0]);
    ui_table_rows(f, game, table, main_sq[0]);
    let ui_indicator = Block::default()
        .title("Stats")
        .borders(Borders::ALL)
        .border_type(BorderType::Rounded);
    f.render_widget(ui_indicator, main_sq[1]);
}
fn ui_table_rows<B: Backend>(
    f: &mut Frame<B>,
    game: &mut Chess,
    table: &[[i32; 8]; 8],
    area: Rect,
) {
    let rows = Layout::default()
        //.margin(3)
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
            ]
            .as_ref(),
        )
        .split(area);
    for i in 0..rows.len() {
        new_table_line(f, game, table, i, rows[i]);
    }
}
fn new_table_line<B: Backend>(
    f: &mut Frame<B>,
    game: &mut Chess,
    table: &[[i32; 8]; 8],
    i: usize,
    area: Rect,
) {
    let line = Layout::default()
        //.margin(3)
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
                Constraint::Ratio(1, 8),
            ]
            .as_ref(),
        )
        .split(area);
    for j in 0..line.len() {
        let icon = match table[i][j] {
            1 => "♙",
            -1 => "♟︎",
            2 => "♖",
            -2 => "♜︎",
            3 => "♘",
            -3 => "♞︎",
            4 => "♗",
            -4 => "♝︎",
            5 => "♕",
            -5 => "♛︎",
            6 => "♔",
            -6 => "♚",
            _ => " ",
        };
        let value = Paragraph::new(icon)
            .block(
                Block::default()
                    .borders(Borders::ALL)
                    .border_type(BorderType::Plain),
            )
            .style(match game.get_mode() {
                Mode::Select => match game.is_pos_sel(i, j) {
                    true => Style::default().bg(Color::Cyan).fg(Color::Black),
                    false => Style::default(),
                },
                Mode::Mark => match game.is_pos_mark(i, j) {
                    true => Style::default().bg(Color::Green).fg(Color::Black),
                    false => Style::default(),
                },
                _ => Style::default(),
            });
        f.render_widget(value, line[j]);
    }
}
